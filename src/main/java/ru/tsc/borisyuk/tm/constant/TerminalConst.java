package ru.tsc.borisyuk.tm.constant;

import org.jetbrains.annotations.NotNull;

public class TerminalConst {

    @NotNull
    public static final String EXIT = "exit";

    @NotNull
    public static final String HELP = "help";

}
