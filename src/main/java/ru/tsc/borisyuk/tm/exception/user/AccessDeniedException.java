package ru.tsc.borisyuk.tm.exception.user;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
