package ru.tsc.borisyuk.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.command.AbstractTaskCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskShowAllFromProjectByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-show-by-project-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks by project id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println(serviceLocator.getProjectTaskService().findAllTaskByProjectId(userId, projectId));
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
