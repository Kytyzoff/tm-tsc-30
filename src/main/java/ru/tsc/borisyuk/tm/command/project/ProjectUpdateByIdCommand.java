package ru.tsc.borisyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.command.AbstractProjectCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectService().existsById(id)) throw new ProjectNotFoundException();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateById(userId, id, name, description);
        Optional.ofNullable(projectUpdated).orElseThrow(ProjectNotFoundException::new);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
