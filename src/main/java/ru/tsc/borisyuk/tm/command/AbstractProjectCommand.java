package ru.tsc.borisyuk.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.exception.empty.EmptyNameException;
import ru.tsc.borisyuk.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.borisyuk.tm.model.Project;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@Nullable Project project) {
        @Nullable final List<Project> projects = serviceLocator.getProjectService().findAll();
        @Nullable final Integer indexNum = projects.indexOf(project) + 1;
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Index: " + indexNum);
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
    }

    @NotNull
    protected Project add(@Nullable final String name, @NotNull final String description) {
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty()) throw new EmptyNameException();
        return new Project(name, description, new Date());
    }

}
