package ru.tsc.borisyuk.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    @Nullable void setStatus(@NotNull Status status);

}
