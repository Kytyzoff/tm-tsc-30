package ru.tsc.borisyuk.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasCreated {

    @Nullable Date getCreated();

    void setCreated(@NotNull Date created);

}
