package ru.tsc.borisyuk.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter

public class Command {

    @NotNull
    private final String name;

    @NotNull
    private final String argument;

    @Nullable
    private final String description;

    public Command(@NotNull String name, @NotNull String argument, @NotNull String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    @Override
    public String toString() {
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name + " ";
        if (argument != null && !argument.isEmpty()) result += "(" + argument + ") ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}

