package ru.tsc.borisyuk.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter

public abstract class AbstractOwnerEntity extends AbstractEntity {

    @NotNull
    protected String userId;

}
